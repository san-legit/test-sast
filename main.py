from flask import Flask, request
import sqlite3

app = Flask(__name__)

@app.route('/')
def index():
    return "Welcome to the vulnerable app!"

@app.route('/search')
def search():
    user_input = request.args.get('name')
    # Vulnerable SQL query
    query = "SELECT * FROM users WHERE name = '" + user_input + "'"
    conn = sqlite3.connect('database.db')
    cursor = conn.cursor()
    cursor.execute(query)
    results = cursor.fetchall()
    cursor.close()
    conn.close()
    return str(results)

if __name__ == '__main__':
    app.run(debug=True)
