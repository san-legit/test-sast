from flask import Flask, request
import sqlite3, os

app = Flask(__name__)

# Vulnerability 1: SQL Injection
@app.route('/search')
def search():
    user_input = request.args.get('name')
    query = "SELECT * FROM users WHERE name = '" + user_input + "'"
    conn = sqlite3.connect('database.db')
    cursor = conn.cursor()
    cursor.execute(query)
    results = cursor.fetchall()
    cursor.close()
    conn.close()
    return str(results)

# Vulnerability 2: Hardcoded Credentials
ADMIN_PASSWORD = "SuperSecretPassword"

# Vulnerability 3: Cross-Site Scripting (XSS)
@app.route('/welcome')
def welcome():
    user_name = request.args.get('username')
    return "<html><body><h1>Welcome, " + user_name + "!</h1></body></html>"

# Vulnerability 4: Path Traversal
@app.route('/get-file')
def get_file():
    file_name = request.args.get('file')
    with open(file_name, 'r') as file:
        return file.read()

# Vulnerability 5: Command Injection
@app.route('/ping')
def ping():
    ip_address = request.args.get('ip')
    result = os.system(f'ping {ip_address}')
    return str(result)

if __name__ == '__main__':
    app.run(debug=True)
